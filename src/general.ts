export interface CurrentPosition {
    x: number;
    y: number;
}

export interface CurrentVelocity {
    x: number;
    y: number;
}